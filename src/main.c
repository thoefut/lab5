#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

static const int YSIZE = 10; //Размеры поля
static const int XSIZE = 10;
static const int dx[4] = {0, 1, 0, -1}; //Вектора сторон света, так удобнее
static const int dy[4] = {-1, 0, 1, 0}; //up right down left
static const int cdx[4] = {1, 1, -1, -1}; //
static const int cdy[4] = {-1, 1, 1, -1};
typedef struct {
    int x, y;
} coords;

/*1 if corrdinates are OK*/
int CheckCoords(coords coordinates) {
    if (coordinates.x < 1 || coordinates.y < 1 || coordinates.x > XSIZE || coordinates.y > YSIZE) {
        return 0;
    }
    return 1;
}

int CountShips(int shipField[YSIZE][XSIZE], int shipStatistics[4]);

void GameInit();

void PrintCoordinates(coords coordinates) {
    printf("%c%i", coordinates.x + 'a' - 1, coordinates.y);
}
//Translates internal fields in nice graphics
void PrintPseudoGraphics(coords coordinates, int shipField[YSIZE][XSIZE]) {
    int i = coordinates.y - 1;
    int j = coordinates.x - 1;
    if (shipField[i][j] == 0) { printf("|%c", '~'); } // Sea tile
    else if (shipField[i][j] == 1) { printf("|%c", '*'); } // Empty tile
    else if (shipField[i][j] == 2) { printf("|%c", '0'); } // Ship tile
    else if (shipField[i][j] == 3) { printf("|%c", 'X'); } // hit ship tile
    else if (shipField[i][j] == 4) { printf("|%c", '#'); } // Dead ship tile
    else { printf("|%c", '?'); } // Unknown tile
}
//Main game screen
void PrintTable(int shipField[YSIZE][XSIZE], int hitField[YSIZE][XSIZE]) {
    printf("0-deck. x-hit deck. #-sunken deck. *-miss|confirmed not a ship\n");
    printf("   ");
    for (int i = 0; i < XSIZE; i++) { printf("%c ", 'a' + i); }
    printf("\t\t\t   ");
    for (int i = 0; i < XSIZE; i++) { printf("%c ", 'a' + i); }
    printf("\n");

    for (int i = 0; i < YSIZE; i++) {
        if (i < 9) {
            printf("%i ", i + 1);
        } else {
            printf("%i", i + 1);
        }
        for (int j = 0; j < XSIZE; j++) {
//          printf(" %i", shipField[i][j]);
            coords graph;
            graph.x = j + 1;
            graph.y = i + 1;
            PrintPseudoGraphics(graph, shipField);
        }
        printf("|");
        printf("\t\t\t");
        if (i < 9) {
            printf("%i ", i + 1);
        } else {
            printf("%i", i + 1);
        }
        for (int j = 0; j < XSIZE; j++) {
//          printf(" %i", hitField[i][j]);
            coords graph;
            graph.x = j + 1;
            graph.y = i + 1;
            PrintPseudoGraphics(graph, hitField);
        }
        printf("|");
        printf("\n");
    }
}

//Fills game field with 0s
void EmptyField(int field[YSIZE][XSIZE]) {
    for (int i = 0; i < YSIZE; i++) {
        for (int j = 0; j < XSIZE; j++) { field[i][j] = 0; }
    }
}

//When ship is hit, diagonal tiles do not contain other ship parts by rules. Makes sure AI handles that
void CrossSurroundWithForbidden(coords coordinates, int field[YSIZE][XSIZE]) {
    for (int i = 0; i < 4; i++) {
        coords crossCoords;
        crossCoords.x = coordinates.x + cdx[i];
        crossCoords.y = coordinates.y + cdy[i];
        if (CheckCoords(crossCoords) == 1) { field[crossCoords.y - 1][crossCoords.x - 1] = 1; }
    }
}
//See SurroundKilledShipWithForbidden
void __SurroundKilledShipWithForbidden(coords coordinates, int field[YSIZE][XSIZE], coords source) {
    CrossSurroundWithForbidden(coordinates, field);
    for (int i = 0; i < 4; ++i) {
        coords checkCoords;
        checkCoords.x = coordinates.x + dx[i];
        checkCoords.y = coordinates.y + dy[i];
        if (!(checkCoords.x == source.x && checkCoords.y == source.y)) {
            if (CheckCoords(checkCoords) == 1) {
                if (field[checkCoords.y - 1][checkCoords.x - 1] == 0) {
                    field[checkCoords.y - 1][checkCoords.x - 1] = 1;
                }
                if (field[checkCoords.y - 1][checkCoords.x - 1] == 4) {
                    __SurroundKilledShipWithForbidden(checkCoords, field, coordinates);
                }
            }
        }
    }
}
//Fills field with forbidden tiles for AI decision making
void SurroundKilledShipWithForbidden(coords coordinates, int field[YSIZE][XSIZE]) {
    __SurroundKilledShipWithForbidden(coordinates, field, coordinates);
}
//Fills field with forbidden tiles as set by rules
void FillWithForbidden(coords coordinates, int field[YSIZE][XSIZE]) {
    int arrayX = coordinates.x - 1;
    int arrayY = coordinates.y - 1;
    int xMin = arrayX - 1;
    int xMax = arrayX + 1;
    int yMin = arrayY - 1;
    int yMax = arrayY + 1;
    if (xMin < 0) { xMin = 0; }
    if (xMax > (XSIZE - 1)) { xMax = XSIZE - 1; }
    if (yMin < 0) { yMin = 0; }
    if (yMax > (YSIZE - 1)) { yMax = YSIZE - 1; }
    for (int j = yMin; j <= yMax; j++) {
        for (int i = xMin; i <= xMax; i++) { field[j][i] = 1; }
    }
}
//Checks if ship would fit given coordinates before placing it
int CheckCoordsForShipFit(int size, coords coordinates, int direction) {
    if (CheckCoords(coordinates) == 0) { return 0; }
    if (coordinates.y + (dy[direction] * (size - 1)) > YSIZE || coordinates.y + (dy[direction] * (size - 1)) < 1 ||
        coordinates.x + (dx[direction] * (size - 1)) > XSIZE ||
        coordinates.x + (dx[direction] * (size - 1)) < 1) { return 0; }
    return 1;
}

/*  PlaceShip
 *  Returns: -1 - ошибка координат. 0 - не удалось. 1 - удалось*/
int PlaceShip(coords coordinates, int size, int direction, int shipField[YSIZE][XSIZE]) {
    if (CheckCoordsForShipFit(size, coordinates, direction) == 0) {
        return -1; /*Coords are out of array*/
    }
    for (int i = 0; i < size; i++) {
        if (shipField[(coordinates.y - 1) + dy[direction] * i][(coordinates.x - 1) + dx[direction] * i] !=
            0) { return 0; }
    }
    for (int i = 0; i < size; i++) {
        coords surroundcordinates;
        surroundcordinates.x = coordinates.x + dx[direction] * i;
        surroundcordinates.y = coordinates.y + dy[direction] * i;
        FillWithForbidden(surroundcordinates, shipField);
    }
    for (int i = 0; i < size; i++) {
        shipField[(coordinates.y - 1) + dy[direction] * i][(coordinates.x - 1) + dx[direction] * i] = 2;
    }
    return 1;
}

//Filling with ships is absolutely random and relies on luck with no guarantee of succesful placement
void FillWithShips(int shipField[YSIZE][XSIZE]) {
    int i = 0;
    int forX[] = {1, 0};
    int forY[] = {0, 1};
    for (int shipSize = 4; shipSize > 0; --shipSize) {
        i = 0;
        while (i < (5 - shipSize)) {
            int direction = rand() % 2 + 1;
            coords coordinates;
            coordinates.x = rand() % (XSIZE - ((shipSize - 1) * forX[direction - 1])) + 1;
            coordinates.y = rand() % (YSIZE - ((shipSize - 1) * forY[direction - 1])) + 1;
            if (PlaceShip(coordinates, shipSize, direction, shipField) == 1) {
                i++;
            }
        }
    }
    int stats[4];
    CountShips(shipField, stats);
    if (stats[0] == 4 && stats[1] == 3 && stats[2] == 2 && stats[3] == 1) {
        return;
    }
    //If placement fails, try again
    EmptyField(shipField);
    FillWithShips(shipField);
}
//CheckFirHit worker function
int _CheckForHitWorker(coords coordinates, int direction, int shipField[YSIZE][XSIZE]) {
    if (coordinates.x < 1 || coordinates.x > XSIZE || coordinates.y < 1 || coordinates.y > YSIZE) {
        return 1;
    }
    if (shipField[coordinates.y - 1][coordinates.x - 1] == 2) { return 0; }
    else if (shipField[coordinates.y - 1][coordinates.x - 1] == 3) {
        coords newcoords;
        newcoords.x = coordinates.x + dx[direction];
        newcoords.y = coordinates.y + dy[direction];
        return _CheckForHitWorker(newcoords, direction, shipField);
    } else { return 1; }
}

/*CheckForHit() - проверяет попадание.
 * -1 - ошибка; 0 - промах; 1 - попадание; 2 - убийство.*/
int CheckForHit(coords coordinates, int shipField[YSIZE][XSIZE]) {
    if (shipField[coordinates.y - 1][coordinates.x - 1] == 3) { return 1; } //Если палуба подбита - возвр. попадание
    if (shipField[coordinates.y - 1][coordinates.x - 1] == 4) { return 2; } //Если палуба убита - возвр. убитую
    if (shipField[coordinates.y - 1][coordinates.x - 1] == 2) {
        for (int i = 0; i < 4; i++) {
            coords newcoords;
            newcoords.x = coordinates.x + dx[i];
            newcoords.y = coordinates.y + dy[i];
            if (_CheckForHitWorker(newcoords, i, shipField) == 0) { return 1; }
        }
        return 2;
    }
    return 0;
}
//Worker function of KillShip
void _Killship(coords coordinates, coords source, int field[YSIZE][XSIZE]) {
    if (CheckCoords(coordinates) == 0) { return; }
    field[coordinates.y - 1][coordinates.x - 1] = 4;
    for (int i = 0; i < 4; ++i) {
        coords nextField;
        nextField.x = coordinates.x + dx[i];
        nextField.y = coordinates.y + dy[i];
        if (!(source.x == nextField.x && source.y == nextField.y)) {
            if (CheckCoords(nextField) == 1) {
                if (field[nextField.y - 1][nextField.x - 1] == 4 || field[nextField.y - 1][nextField.x - 1] == 3 ||
                    field[nextField.y - 1][nextField.x - 1] == 2) {
                    _Killship(nextField, coordinates, field);
                }
            }
        }
    }
}
//Calls worker function to change hit ship tiles to sunk
void KillShip(coords coordinates, int shipField[YSIZE][XSIZE]) {
    _Killship(coordinates, coordinates, shipField);
}
//Function that checks for damage on coordinates
int Hit(coords coordinates, int enemyShipField[YSIZE][XSIZE]) {
    int hit = CheckForHit(coordinates, enemyShipField);
    if (hit == 0) {
        return 0;
    }
    if (hit == 2) {
        enemyShipField[coordinates.y - 1][coordinates.x - 1] = 4;
        KillShip(coordinates, enemyShipField);
        SurroundKilledShipWithForbidden(coordinates, enemyShipField);
        return 2;
    }
    if (hit == 1) {
        enemyShipField[coordinates.y - 1][coordinates.x - 1] = 3;
        return 1;
    }
    return 0; //safety
}
//To reveal ships for cheat code
void RevealShips(int hitField[YSIZE][XSIZE], int shipField[YSIZE][XSIZE]) {
    for (int i = 0; i < YSIZE; ++i) {
        for (int j = 0; j < XSIZE; ++j) {
            if (shipField[i][j] == 2) { hitField[i][j] = 2; }
            if (shipField[i][j] == 3) { hitField[i][j] = 3; }
            if (shipField[i][j] == 4) { hitField[i][j] = 4; }
        }
    }
}

/*coords GetCoordinatesFromKeyboard() {
    coords a;
    char buffer[15];
    printf("Please, enter coordinates\n");
    char y = 0;
    int x = 0;
    fgets(buffer, 5, stdin);
    sscanf(buffer, "%c%i", &y, &x);
    a.y = y + 1 - 'a';
    a.x = x;
    return a;
}*/

//Changes empty tiles to sea tiles for graphics
void ClearField(int field[YSIZE][XSIZE]) {
    for (int i = 0; i < YSIZE; ++i) {
        for (int j = 0; j < XSIZE; ++j) {
            if (field[i][j] == 1) { field[i][j] = 0; }
        }
    }
}
//Handles player turn
int PlayerTurn(int hitField[YSIZE][XSIZE], int enemyShipField[YSIZE][XSIZE], int playerShipField[YSIZE][XSIZE],
               int shipsLeftP1, int shipsLeftP2, int *shipStats1, int *shipStats2) {
    //printf("\n\n\n\n\n\n");
    //printf("------------------------------\n");
    PrintTable(playerShipField, hitField);
    printf("\tShips left: %i\t\t\t\t\tShips left: %i\n", shipsLeftP1, shipsLeftP2);//\t everywhere!
    printf("S1: %i,S2: %i,S3: %i,S4: %i\t\t\t", shipStats1[0], shipStats1[1], shipStats1[2], shipStats1[3]);
    printf("S1: %i,S2: %i,S3: %i,S4: %i\n ", shipStats2[0], shipStats2[1], shipStats2[2], shipStats2[3]);
    coords hitCoordinates; //= GetCoordinatesFromKeyboard();
    char buffer[10];
    char cheat[] = "why\n";
    char empty[] = "\n";
    printf("Please, enter coordinates\n");
    char x = 0;
    int y = 0;
    fgets(buffer, 10, stdin);
    if (strcmp(buffer, cheat) == 0) {
        RevealShips(hitField, enemyShipField);
        PrintTable(playerShipField, hitField);
        printf("Enemy ships revealed!\nPlease, enter coordinates\n");
        printf("\tShips left: %i\t\t\t\t\tShips left: %i\n", shipsLeftP1, shipsLeftP2);//\t everywhere!
        printf("S1: %i,S2: %i,S3: %i,S4: %i\t\t\t", shipStats1[0], shipStats1[1], shipStats1[2], shipStats1[3]);
        printf("S1: %i,S2: %i,S3: %i,S4: %i\n ", shipStats1[0], shipStats1[1], shipStats1[2], shipStats1[3]);
        fgets(buffer, 10, stdin);
    }
    if (strcmp(buffer, empty) == 0) {
        printf("Empty input. Skipping turn\n");
        return 0;
    }
    sscanf(buffer, "%c%i", &x, &y);
    if (x + 1 - 'a' < XSIZE + 1 && x + 1 - 'a' > 0) {
        hitCoordinates.x = (x + 1 - 'a');
    } else if (x + 1 - 'A' < XSIZE + 1 && x + 1 - 'A' > 0) {
        hitCoordinates.x = (x + 1 - 'A');
    } else {
        printf("Wrong input. Skipping turn\n");
        return 0;
    }

    hitCoordinates.y = y;
    int hit = Hit(hitCoordinates, enemyShipField);
    if (hit == 0) {
        printf("You missed a ship on ");
        PrintCoordinates(hitCoordinates);
        printf("\n");
        hitField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 1;
        return 0;
    } else if (hit == 1) {
        printf("You hit a ship!\n");
        PrintCoordinates(hitCoordinates);
        printf("\n");
        hitField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 3;
        return 1;
    } else if (hit == 2) {
        printf("You killed a ship!\n");
        PrintCoordinates(hitCoordinates);
        printf("\n");
        hitField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 4;
        KillShip(hitCoordinates, hitField);
        SurroundKilledShipWithForbidden(hitCoordinates, hitField);
        return 1;
    }
    return 0;
}
//Selects random coords that is not yet been checked
coords GetHitCoordsOnUnchecked(int hitField[YSIZE][XSIZE]) {
    int possibleHitsLeft = 0;
    coords hitCoordinates = {1, 1};
    for (int i = 0; i < YSIZE; ++i) {
        for (int j = 0; j < XSIZE; ++j) {
            if (hitField[i][j] == 0) {
                possibleHitsLeft++;
            }
        }
    }
    if (possibleHitsLeft != 0) {
        int hit = 1 + rand() % possibleHitsLeft;
        for (int i = 0; i < YSIZE; ++i) {
            for (int j = 0; j < XSIZE; ++j) {
                if (hitField[i][j] == 0) {
                    hit--;
                }
                if (hit == 0) {
                    hitCoordinates.x = j + 1;
                    hitCoordinates.y = i + 1;
                    return hitCoordinates;
                }
            }
        }
    }
    hitCoordinates.x = 1 + rand() % XSIZE;
    hitCoordinates.y = 1 + rand() % YSIZE;
    return hitCoordinates;
}
//Check if some known ships are not sunk
int CheckForNotFinishedShips(int hitField[YSIZE][XSIZE]) {
    for (int i = 0; i < YSIZE; ++i) {
        for (int j = 0; j < XSIZE; ++j) {
            if (hitField[i][j] == 2 || hitField[i][j] == 3) {
                return 1;
            }
        }
    }
    return 0;
}
//Element swapper 
void Swap(int *a, int *b) {
    int temp = *b;
    *b = *a;
    *a = temp;
}
//Array shuffler
void Shuffle(int *array, int arraySize) {
    srand((unsigned int) clock());
    for (int i = 0; i < arraySize; i++) {
        Swap(&array[i], &array[rand() % (arraySize - i) + i]);
    }
}
//Coords guesser for ai to finish hit ship
coords GetCoordsToFinishShip(int hitField[YSIZE][XSIZE]) {
    int randomseed[] = {0, 1, 2, 3};
    Shuffle(randomseed, 4);
    coords hitCoordinates = {1, 1};
    for (int i = 0; i < YSIZE; ++i) {
        for (int j = 0; j < XSIZE; ++j) {
            if (hitField[i][j] == 2) {
                hitCoordinates.x = j + 1;
                hitCoordinates.y = i + 1;
                return hitCoordinates;
            }
        }
    }
    for (int i = 0; i < YSIZE; ++i) {
        for (int j = 0; j < XSIZE; ++j) {
            if (hitField[i][j] == 3) {
                for (int k = 0; k < 4; k++) {
                    coords hitCandidate;
                    hitCandidate.x = j + 1 + dx[k];
                    hitCandidate.y = i + 1 + dy[k];
                    if (CheckCoords(hitCandidate) == 1) {
                        if (hitField[hitCandidate.y - 1][hitCandidate.x - 1] == 0) {
                            hitCoordinates.x = hitCandidate.x;
                            hitCoordinates.y = hitCandidate.y;
                            return hitCoordinates;
                        }
                    }
                }
            }
        }
    }
    return hitCoordinates;
}
//Ai check for ships that it can finish if possible, if there are none randomly guesses
//Stdouts results
int AiTurn(int hitField[YSIZE][XSIZE], int enemyShipField[YSIZE][XSIZE]) {
    coords hitCoordinates;
    if (CheckForNotFinishedShips(hitField) == 1) {
        hitCoordinates = GetCoordsToFinishShip(hitField);
    } else {
        hitCoordinates = GetHitCoordsOnUnchecked(hitField);
    }
    int hit = Hit(hitCoordinates, enemyShipField);
    printf("AI shoots ");
    PrintCoordinates(hitCoordinates);
    if (hit == 0) {
        hitField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 1;
        enemyShipField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 1;
        printf(" ..and misses!\n");
        return 0;
    } else if (hit == 1) {
        CrossSurroundWithForbidden(hitCoordinates, hitField);
        hitField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 3;
        printf(" ..and hits a ship!\n");
        return 1;
    } else if (hit == 2) {
        CrossSurroundWithForbidden(hitCoordinates, hitField);
        hitField[hitCoordinates.y - 1][hitCoordinates.x - 1] = 4;
        KillShip(hitCoordinates, hitField);
        SurroundKilledShipWithForbidden(hitCoordinates, hitField);
        printf(" ..and sinks your ship!\n");
        return 1;
    }
    return 0;
}

int _GetShipSizeOnFieldWorker(coords coordinates, int direction, int counter, int shipField[YSIZE][XSIZE]) {
    if (CheckCoords(coordinates) == 0) { return counter; }
    if (!(shipField[coordinates.y - 1][coordinates.x - 1] == 2 ||
          shipField[coordinates.y - 1][coordinates.x - 1] == 3)) { return counter; }
    counter++;
    coords newcoords;
    newcoords.x = coordinates.x + dx[direction];
    newcoords.y = coordinates.y + dy[direction];
    return _GetShipSizeOnFieldWorker(newcoords, direction, counter, shipField);
}

int GetShipSizeOnField(coords coordinates, int shipField[YSIZE][XSIZE]) {
    if (CheckCoords(coordinates) == 0) { return 0; }
    if (!(shipField[coordinates.y - 1][coordinates.x - 1] == 2 ||
          shipField[coordinates.y - 1][coordinates.x - 1] == 3)) { return 0; }
    int shipSize = 1;
    for (int i = 0; i < 4; i++) {
        coords newcoords;
        newcoords.x = coordinates.x + dx[i];
        newcoords.y = coordinates.y + dy[i];
        if (CheckCoords(newcoords)) { shipSize += _GetShipSizeOnFieldWorker(newcoords, i, 0, shipField); }
    }
    return shipSize;
}

void _MaskTheRestOfTheShipWorker(coords coordinates, int direction, int shipField[YSIZE][XSIZE],
                                 int maskField[YSIZE][XSIZE]) {
    if (CheckCoords(coordinates) && (shipField[coordinates.y - 1][coordinates.x - 1] == 2 ||
                                     shipField[coordinates.y - 1][coordinates.x - 1] == 3)) {
        maskField[coordinates.y - 1][coordinates.x - 1] = 1;
        coords newcoords;
        newcoords.x = coordinates.x + dx[direction];
        newcoords.y = coordinates.y + dy[direction];
        if (CheckCoords(newcoords) == 1) { _MaskTheRestOfTheShipWorker(newcoords, direction, shipField, maskField); }
    }
}

void MaskTheShip(coords coordinates, int shipField[YSIZE][XSIZE], int maskField[YSIZE][XSIZE]) {
    if (shipField[coordinates.y - 1][coordinates.x - 1] == 2 ||
        shipField[coordinates.y - 1][coordinates.x - 1] == 3) { maskField[coordinates.y - 1][coordinates.x - 1] = 1; }
    for (int i = 0; i < 4; i++) {
        coords newcoords;
        newcoords.x = coordinates.x + dx[i];
        newcoords.y = coordinates.y + dy[i];
        if (CheckCoords(newcoords) == 1) {
            if (shipField[coordinates.y - 1 + dy[i]][coordinates.x - 1 + dx[i]] == 2 ||
                shipField[coordinates.y - 1 + dy[i]][coordinates.x - 1 + dx[i]] == 3) {
                _MaskTheRestOfTheShipWorker(newcoords, i, shipField, maskField);
            }
        }
    }
}
//Ask if player would like to start a new game
void ComplyNewGame() {
    char buffer[5];
    char yes1[] = "yes\n";
    char yes2[] = "y\n";
    char yes3[] = "Y\n";
    printf("Start a new game? (Yes)(No)\n");
    fgets(buffer, 5, stdin);
    if (strcmp(buffer, yes1) == 0 || strcmp(buffer, yes2) == 0 || strcmp(buffer, yes3) == 0) {
        GameInit();
    }
}
//Count how many ships are left on field
int CountShips(int shipField[YSIZE][XSIZE], int shipStatistics[]) {
    int count = 0;
    int maskField[YSIZE][XSIZE];
    EmptyField(maskField);
    for (int i = 0; i < 4; i++) {
        shipStatistics[i] = 0;
    }
    for (int i = 0; i < YSIZE; i++) {
        for (int j = 0; j < XSIZE; j++) {
            if (maskField[i][j] == 0 && (shipField[i][j] == 2 || shipField[i][j] == 3)) {
                count++;
                coords coordinates;
                coordinates.x = j + 1;
                coordinates.y = i + 1;
                shipStatistics[GetShipSizeOnField(coordinates, shipField) - 1] += 1;
                MaskTheShip(coordinates, shipField, maskField);
            }
        }
    }
    return count;
}

int Game(int player1ShipField[YSIZE][XSIZE], int player2ShipField[YSIZE][XSIZE], int player1HitField[YSIZE][XSIZE],
         int player2HitField[YSIZE][XSIZE]) {
    int gameOver = 0;
    int shipStatisticsField1[4];
    int shipStatisticsField2[4];
    int WINNER = 0;
    int TURN = 1; //0|1
    int p1Turns = 0;
    int p2Turns = 2;
    while (1) {
        int shipsLeftP1 = CountShips(player1ShipField, shipStatisticsField1);
        int shipsLeftP2 = CountShips(player2ShipField, shipStatisticsField2);
        if (shipsLeftP1 == 0) {
            gameOver = 1;
            WINNER = 2;
        }
        if (shipsLeftP2 == 0) {
            gameOver = 1;
            WINNER = 1;
        }
        if (gameOver == 1) {
            break;
        }
        if (TURN == 1) {
            p1Turns += 1;
            //ClearScreen();

            if (PlayerTurn(player1HitField, player2ShipField, player1ShipField, shipsLeftP1, shipsLeftP2,
                           shipStatisticsField1, shipStatisticsField2) == 0) {
                TURN = 2;
            }
        }
        if (TURN == 2) {
            p2Turns += 1;
            if (AiTurn(player2HitField, player1ShipField) == 0) {
                TURN = 1;
            }
        }
    }
    PrintTable(player1ShipField, player2ShipField);
    int winnerShipsLeft;
    if (WINNER == 1) {
        winnerShipsLeft = CountShips(player1ShipField, shipStatisticsField1);
    } else {
        winnerShipsLeft = CountShips(player2ShipField, shipStatisticsField2);
    }
    printf("\n\n \tGAME OVER \nPlayer%i won. Game took %i (p1:%i p2:%i) turns. Winner got %i ships left\n", WINNER,
           p1Turns + p2Turns, p1Turns, p2Turns,
           winnerShipsLeft);
    if (WINNER == 1) {
        printf("Statistics on ships left:\n");
        printf("S1: %i,S2: %i,S3: %i,S4: %i\n ", shipStatisticsField1[0], shipStatisticsField1[1],
               shipStatisticsField1[2], shipStatisticsField1[3]);
    } else {
        printf("Statistics on ships left:\n");
        printf("S1: %i,S2: %i,S3: %i,S4: %i\n ", shipStatisticsField2[0], shipStatisticsField2[1],
               shipStatisticsField2[2], shipStatisticsField2[3]);
    }
    return p1Turns + p2Turns;
}

int CheckCorners(coords coordinates, int field[YSIZE][XSIZE]) {
    for (int i = 0; i < 4; i++) {
        if (coordinates.x + cdx[i] > 0 && coordinates.x + cdx[i] < (XSIZE + 1) && coordinates.y + cdy[i] > 0 &&
            coordinates.y + cdy[i] < (YSIZE + 1)) {
            if (field[(coordinates.y - 1) + cdy[i]][(coordinates.x - 1) + cdx[i]] == 2) {
                return 0;
            }
        }
    }
    return 1;
}

void CreateInputFile() {
    FILE *file = fopen("input", "w");
    int field[YSIZE][XSIZE];
    EmptyField(field);
    FillWithShips(field);
    ClearField(field);
    for (int i = 0; i < YSIZE; i++) {
        for (int j = 0; j < XSIZE; j++) {
            if (field[i][j] == 2) { fprintf(file, "%c", '1'); }
            if (field[i][j] == 0) { fprintf(file, "%c", '0'); }
        }
        fprintf(file, "\n");
    }
    fclose(file);
}

void ReadFieldFromFile(int field[YSIZE][XSIZE]) {
    FILE *inputfile = fopen("input", "r");
    if (inputfile == NULL) {
        printf("No file found. Creating and randomly filling\n");
        CreateInputFile();
    }
    inputfile = fopen("input", "r");
    for (int i = 0; i < YSIZE; i++) {
        for (int j = 0; j < XSIZE; j++) {
            char a = 0;
            fscanf(inputfile, "%c", &a);
            if (a == '1') { field[i][j] = 2; }
            if (a == '0') { field[i][j] = 0; }
        }
        char rubbish;
        fscanf(inputfile, "%c", &rubbish); //Seek alternative?
    }
    fclose(inputfile);
    for (int k = 0; k < YSIZE; ++k) {
        for (int i = 0; i < XSIZE; ++i) {
            if (field[k][i] == 2) {
                coords checkcoords;
                checkcoords.x = i + 1;
                checkcoords.y = k + 1;
                if (CheckCoords(checkcoords) == 1) {
                    if (CheckCorners(checkcoords, field) == 0) {
                        printf("Bad ship placement in file!\nShips must not connect on corners\n");
                        PrintTable(field, field);
                        exit(1);
                    }
                }
            }
        }
    }
    int normalStats[4] = {4, 3, 2, 1};
    int stats[4] = {0, 0, 0, 0};
    CountShips(field, stats);
    for (int i = 0; i < 4; i++) {
        if (stats[i] != normalStats[i]) {
            printf("Bad ship placement!\nYou must have 4 size 1 ships, 3 size 2 ships, 2 size 3 ships, 1 size 4.");
            printf("Yours are:\n");
            for (int j = 0; j < 4; j++) { printf("Size %i - %i\t", j + 1, stats[i]); }
            printf("\n");
            exit(1);
        }
    }
}

void FillPlayerField(int field[YSIZE][XSIZE]) {
    printf("Do you want to input ships from file, or randomly fill your field? (1-file, 2-RNG gods)\n");
    printf("Input file is located in program's directory, input file will be generated if not found\n");
    int variant = -1;
    char buffer[10];
    do {
        fgets(buffer, 10, stdin);
        sscanf(buffer, "%i", &variant);
    } while (variant != 2 && variant != 1);
    if (variant == 1) {
        ReadFieldFromFile(field);
    }
    if (variant == 2) {
        FillWithShips(field);
    }
}

void GameInit() {
    srand((unsigned int) time(NULL)); //get random seed
    int player1ShipField[YSIZE][XSIZE]; //declare fields for players
    int player1HitField[YSIZE][XSIZE];
    int player2ShipField[YSIZE][XSIZE];
    int player2HitField[YSIZE][XSIZE];
    EmptyField(player1ShipField); //make sure they are zeroed
    EmptyField(player1HitField);
    EmptyField(player2ShipField);
    EmptyField(player2HitField);
    FillPlayerField(player1ShipField); //prompt to choose filloption
    FillWithShips(player2ShipField); //randomlyfill for AI
    ClearField(player1ShipField); //Change empty tiles to sea tiles
    ClearField(player2ShipField); 
    Game(player1ShipField, player2ShipField, player1HitField, player2HitField);
    ComplyNewGame();
}

int main() {
    GameInit();
    return 0;
}
