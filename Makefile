#makefile for game executable named "game"
CFLAGS := $(CFLAGS) -c -Wall -Werror -std=c99 -Og
OBJECTS=main.o
target=game
SRCDIR=src
INPUTFILENAME=input
CC ?= gcc
LD ?= gcc

#Make sure to use DEL for windows as RM
RM ?= /bin/rm -f

all: game
#create the executable
game : $(OBJECTS)
	$(CC) -o $(target) $(OBJECTS)
#create the objects for executable
main.o :
	$(CC) $(CFLAGS) $(SRCDIR)/main.c
#rule to clean all files
.PHONY : clean
clean :
	$(RM) $(target) $(OBJECTS) $(INPUTFILENAME)