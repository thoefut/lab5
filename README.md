### NOTE
Project uses strict c99 procedural style to comply with cource's requirements

### Requirements

* C99 compliant compiler like `gcc` or `clang`
* (optional) `Make` to utilize Makefile

Makefile assumes unix environment

Tested compiling and working on 

```
Linux arch 4.20.10-arch1-1-ARCH

gcc version 8.2.1 20181127 (GCC)
GNU Make 4.2.1
```



### Compiling
If you are running unix system you should be able to use Makefile

```
make clean all
```
Would remove previous compliations and place game executable named "game" in project directory


To run the game you can use.
```
./game
```
Alternative:
Project only contains one source file main.c in src/ directory. You can manually compile it with your favorite C compiler.


### Setting your ships

You will get prompted to select starting field from file.
This is used to have same stard instead of random with each game.
If file named "input" is missing, it will be generated in current directory
when file input option is selected.

You can create or edit this input file to place your ships. 
Ships are stored in 10x10 matrix, 1s indicating ship tile, 0s sea tiles
Reading 10 characters and newline for each row

File is checked for correct ship placement
### Controls


When prompted, enter character and number (ex. "a2" **case sensitive**) indicating position on a field to fire

Results of your turn and AI's will be printed on top of game fields

If you want to cheat and reveal enemy field, enter `why` into the input field while the game is running and press enter button